package com.weixiao.mapper;

import com.weixiao.domain.User;

import java.util.List;

public interface UserMapper {
    //获取所有用户
    public List<User> findUserAll();

    //根据id查询用户
    public User findUserById(User user);

    //添加用户
    public Integer insertUser(User user);

    //修改用户
    public Integer updateUser(User user);
}
