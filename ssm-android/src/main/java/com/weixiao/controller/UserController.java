package com.weixiao.controller;

import com.weixiao.domain.User;
import com.weixiao.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/find")
    public List<User> findUserAll() {
        System.out.println(userService.findUserAll());
        return userService.findUserAll();
    }

    @GetMapping("/findById")
    public User findUserById(User user) {
        System.out.println(userService.findUserById(user));
        return userService.findUserById(user);
    }

    @GetMapping("/insert")
    public Integer insertUser(User user) {
        return userService.insertUser(user);
    }

    @GetMapping("/update")
    public Integer updateUser(User user) {
        return userService.updateUser(user);
    }
}
