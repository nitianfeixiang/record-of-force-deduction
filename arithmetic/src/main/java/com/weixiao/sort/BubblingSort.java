package com.weixiao.sort;

import java.util.Scanner;

import static java.util.Arrays.sort;

public class BubblingSort {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] a = new int[5];
        for (int i = 0; i < 5; i++) {
            a[i] = scanner.nextInt();
        }

        //java自带的排序包装
        //sort(a);
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4 - i; j++) {
                if (a[j] > a[j + 1]) {
                    int sort = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = sort;
                }
            }
        }
        for (int i = 0; i < 5; i++) {
            System.out.println(a[i]);
        }
    }
}
