package com.weixiao.leetcode;

import java.util.HashMap;
import java.util.Scanner;

/*
赎金信:
给你两个字符串：ransomNote 和 magazine ，判断 ransomNote 能不能由 magazine 里面的字符构成。
如果可以，返回 true ；否则返回 false 。
magazine 中的每个字符只能在 ransomNote 中使用一次。
 */
public class Test2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入字符串：");
        String ransomNote = scanner.next();
        System.out.println("请输入拼接字符串：");
        String magazine = scanner.next();
        System.out.println(canConstruct(ransomNote, magazine));
    }
    public static boolean canConstruct(String ransomNote, String magazine) {
        HashMap<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < magazine.length(); i++) {
            //获取字符串中的每个字符，并将每个字符在串中的个数记录在map中
            Character x = magazine.charAt(i);
            if (map.get(x) == null) {
                map.put(x, 1);
            } else {
                map.put(x, map.get(x) + 1);
            }
        }
        for (int i = 0; i < ransomNote.length(); i++) {
            //遍历目标字符串，并且从提供字符的字符串中找到对应的字符，如果没有此字符或者字符已经被用完则直接返回false，否则将数量减一
            Character x=ransomNote.charAt(i);
            Integer size = map.get(x);
            if (size == null || size == 0) {
                return false;
            } else {
                map.put(x, map.get(x) - 1);
            }
        }
        //循环结束，表示所有的字符都找到了，返回true
        return true;
    }
}
