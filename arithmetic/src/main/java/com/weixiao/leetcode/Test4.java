package com.weixiao.leetcode;

import java.util.Scanner;

/**
 * 将数字变成 0 的操作次数:
 * 给你一个非负整数 num ，请你返回将它变成 0 所需要的步数。 如果当前数字是偶数，你需要把它除以 2 ；否则，减去 1 。
 */
public class Test4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入数字：");
        int num = scanner.nextInt();
        System.out.println(numberOfSteps(num));
    }
    public static int numberOfSteps(int num) {
        int k=0;
        while (num != 0) {
            if (num % 2 == 0) {
                num=num/2;
            }else {
                num=num-1;
            }
            k=k+1;
        }
        return k;
    }
}
